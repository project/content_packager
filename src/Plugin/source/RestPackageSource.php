<?php

namespace Drupal\content_packager\Plugin\source;

use Drupal\content_packager\Plugin\PackageSourceBase;

/**
 * REST Export Source Plugin for Content Packager.
 *
 * @PackageSource(
 *   id = "rest",
 *   title = @Translation("Rest Export"),
 *   forms = {
 *     "package" = "\Drupal\content_packager\PluginForm\RestSourcePackage",
 *   }
 * )
 */
class RestPackageSource extends PackageSourceBase {
}
