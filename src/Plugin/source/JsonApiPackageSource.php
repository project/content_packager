<?php

namespace Drupal\content_packager\Plugin\source;

use Drupal\content_packager\Plugin\PackageSourceBase;

/**
 * JSON:API Source Plugin for Content Packager.
 *
 * @PackageSource(
 *   id = "jsonapi",
 *   title = @Translation("JSON:API"),
 *   forms = {
 *     "package" = "\Drupal\content_packager\PluginForm\JsonApiSourcePackage",
 *     "configure" = "\Drupal\content_packager\PluginForm\JsonApiSourceConfig",
 *   }
 * )
 */
class JsonApiPackageSource extends PackageSourceBase {
}
