<?php

namespace Drupal\content_packager\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin Manager for Content Packager module.
 *
 * @see \Drupal\content_packager\Annotation\PackageSource
 * @see \Drupal\content_packager\Plugin\PackageSourceInterface
 * @see \Drupal\content_packager\Plugin\PackageSourceBase
 * @package Drupal\content_packager\Plugin
 */
class SourcePluginManager extends DefaultPluginManager {

  /**
   * SourcePluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend instance to use.
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler, CacheBackendInterface $cacheBackend) {
    parent::__construct(
      'Plugin/source',
      $namespaces,
      $module_handler,
      'Drupal\content_packager\Plugin\PackageSourceInterface',
      'Drupal\content_packager\Annotation\PackageSource'
    );

    $this->setCacheBackend(
      $cacheBackend,
      'content_packager_info_plugins'
    );
  }

}
