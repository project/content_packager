<?php

namespace Drupal\content_packager\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;

/**
 * Defines an interface for Content Packager source implementations.
 *
 * A source generates JSON data and provides functionality that produces
 * files related to the JSON data.
 *
 * @see \Drupal\content_packager\Annotation\PackageSource
 * @see \Drupal\content_packager\Plugin\SourcePluginSettingsBase
 * @see \Drupal\content_packager\Plugin\SourcePluginManager
 * @see plugin_api
 */
interface PackageSourceInterface extends PluginWithFormsInterface {

  /**
   * Retrieves the package form for this plugin.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state for entire "Create package" form.
   *
   * @return \Drupal\Core\Plugin\PluginFormInterface
   *   The "create package" plugin form for the source.
   */
  public function getPackageForm(array $form, FormStateInterface $form_state);

  /**
   * Validates the package form for this plugin.
   *
   * @return array[]
   *   The Batch operations necessary to package this data source.
   */
  public function validatePackageForm(array $form, FormStateInterface $form_state);

  /**
   * Retrieves the package form for this plugin.
   *
   * @return array[]
   *   The Batch operations necessary to package this data source.
   */
  public function submitPackageForm(array $form, FormStateInterface $form_state);

  /**
   * Retrieves the configure form for this plugin.
   *
   * @param array $form
   *   The "Configure Package" form specific to this data source.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state for entire "Create package" form.
   *
   * @return \Drupal\Core\Plugin\PluginFormInterface
   *   The configure plugin form for the source.
   */
  public function getConfigureForm(array $form, FormStateInterface $form_state);

  /**
   * Validates the configure form for this plugin.
   *
   * @return array[]
   *   The Batch operations necessary to package this data source.
   */
  public function validateConfigureForm(array $form, FormStateInterface $form_state);

  /**
   * Submits the configure form for this plugin.
   *
   * @param array $form
   *   The configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state for entire "Create package" form.
   */
  public function submitConfigureForm(array $form, FormStateInterface $form_state);

}
