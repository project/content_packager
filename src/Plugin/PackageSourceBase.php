<?php

namespace Drupal\content_packager\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base implementation for a Package Source plugin.
 *
 * @see \Drupal\content_packager\Annotation\PackageSource
 * @see \Drupal\Core\Action\ActionManager
 * @see \Drupal\Core\Action\ActionInterface
 * @see plugin_api
 * @package Drupal\content_packager\Plugin
 */
abstract class PackageSourceBase extends PluginBase implements PackageSourceInterface, ContainerFactoryPluginInterface {
  use PluginWithFormsTrait;

  /**
   * The plugin form factory service.
   *
   * @var \Drupal\Core\Plugin\PluginFormFactoryInterface
   */
  private $pluginFormFactory;

  /**
   * SourceBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Plugin\PluginFormFactoryInterface $plugin_form_factory
   *   Plugin form factory to discover and instantiate plugin forms.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PluginFormFactoryInterface $plugin_form_factory) {
    $this->pluginFormFactory = $plugin_form_factory;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('plugin_form.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPackageForm(array $form, FormStateInterface $form_state) {
    if (!$this->hasFormClass('package')) {
      return $form;
    }

    $formPlugin = $this->pluginFormFactory->createInstance($this, 'package');
    return $formPlugin->buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validatePackageForm(array $form, FormStateInterface $form_state) {
    if (!$this->hasFormClass('package')) {
      return;
    }

    $formPlugin = $this->pluginFormFactory->createInstance($this, 'package');
    return $formPlugin->validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitPackageForm(array $form, FormStateInterface $form_state) {
    if (!$this->hasFormClass('package')) {
      return;
    }

    $formPlugin = $this->pluginFormFactory->createInstance($this, 'package');
    return $formPlugin->submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigureForm(array $form, FormStateInterface $form_state) {
    if (!$this->hasFormClass('configure')) {
      return $form;
    }

    $formPlugin = $this->pluginFormFactory->createInstance($this, 'configure');
    return $formPlugin->buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigureForm(array $form, FormStateInterface $form_state) {
    if (!$this->hasFormClass('configure')) {
      return;
    }

    $formPlugin = $this->pluginFormFactory->createInstance($this, 'configure');
    return $formPlugin->validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigureForm(array $form, FormStateInterface $form_state) {
    if (!$this->hasFormClass('configure')) {
      return $form;
    }
    $formPlugin = $this->pluginFormFactory->createInstance($this, 'configure');
    return $formPlugin->submitConfigurationForm($form, $form_state);
  }

}
