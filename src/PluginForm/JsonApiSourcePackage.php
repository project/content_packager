<?php

namespace Drupal\content_packager\PluginForm;

use Drupal\Component\Serialization\Json;
use Drupal\content_packager\JsonApiHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBaseTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Plugin\PluginFormBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * JSON:API Plugin's "Package" form.
 *
 * @package Drupal\content_packager\PluginForm
 */
class JsonApiSourcePackage extends PluginFormBase implements ContainerInjectionInterface {

  use ConfigFormBaseTrait;
  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * JsonApiSourcePackage constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->setConfigFactory($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container
      ->get('config.factory')
    );
  }

  /**
   * Sets the config factory for this form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   *
   * @return $this
   */
  public function setConfigFactory(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['content_packager.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('content_packager.settings');
    $url = $config->get('jsonapi_uri');
    $form['make_package']['json_api']['url'] = [
      '#type' => 'select',
      '#title' => $this->t('JSON:API url'),
      '#options' => ['default' => $url], // This will be expanded to more options in the future.
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

    // Currently are ignoring form state; there is only ever one option.
    // That single option is defined on the config form.
    $config = $this->config('content_packager.settings');
    $selected_uri = $config->get('jsonapi_uri');

    $package_uri = $form['#package_uri'];
    $filename_prefix = 'jsonapi-';
    $json_api_ops = $this->buildJsonApiOperations($selected_uri, $package_uri, $filename_prefix);
    if (!$json_api_ops) {
      return [];
    }
    return $json_api_ops;
  }

  /**
   * Helper that assembles the operations necessary for a Views package.
   */
  private function buildJsonApiOperations($source_uri, $dest_uri, $filename_prefix) {
    $operations = [];

    $operations[] = [
      'Drupal\content_packager\BatchOperations::renderAndSaveJsonApiOutput',
      [$source_uri, $dest_uri . DIRECTORY_SEPARATOR, $filename_prefix, '.json'],
    ];

    $image_styles = array_keys($this->config('content_packager.settings')->get('image_styles'));
    $field_blacklist = $this->config('content_packager.settings')->get('fields_ignored');
    $data_uri = $dest_uri . DIRECTORY_SEPARATOR . $filename_prefix;

    $options = [
      'image_styles' => $image_styles,
      'field_blacklist' => $field_blacklist,
      'data_path' => $data_uri,
    ];

    $pages = JsonApiHelper::retrievePagesFromUri($source_uri);
    if (!$pages) {
      return $operations;
    }
    foreach ($pages as $page) {
      $json = Json::decode($page);
      if (!$json) {
        $msg = $this->t('JSON not found at: :uri.', [':uri' => $source_uri]);
        $this->messenger()->addError($msg);
        return FALSE;
      }

      foreach ($json['data'] as $entity_data) {
        $infos = [];
        $id = $entity_data['attributes']['drupal_internal__nid'];
        list($type) = explode('--', $entity_data['type']);
        $infos[] = ['id' => $id, 'type' => $type];

        $package_uri = content_packager_package_uri();

        $operations[] = [
          'Drupal\content_packager\BatchOperations::copyEntityFiles',
          [
            $infos,
            $package_uri,
            $options,
          ],
        ];
      }
    }
    return $operations;
  }

}
