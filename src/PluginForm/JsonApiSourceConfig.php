<?php

namespace Drupal\content_packager\PluginForm;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBaseTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * JSON:API Plugin's "Configure" form.
 *
 * @package Drupal\content_packager\PluginForm
 */
class JsonApiSourceConfig extends PluginFormBase implements ContainerInjectionInterface {

  use ConfigFormBaseTrait;
  use StringTranslationTrait;

  /**
   * JsonApiSourcePackage constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->setConfigFactory($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container
      ->get('config.factory')
    );
  }

  /**
   * Sets the config factory for this form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   *
   * @return $this
   */
  public function setConfigFactory(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['content_packager.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('content_packager.settings');

    $form['make_package']['json_api']['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('JSON:API url'),
      '#default_value' => $config->get('jsonapi_uri'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $json_api_uri = $form_state->getValue('url');
    $config = $this->config('content_packager.settings');
    $prev_uri = $config->get('jsonapi_uri');

    if ($prev_uri !== $json_api_uri) {
      $config->set('jsonapi_uri', $json_api_uri);
      $config->save(TRUE);
    }
  }

}
