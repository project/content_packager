<?php

namespace Drupal\content_packager\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Plugin annotation for Content Packager package source plugins.
 *
 * For a working example, see JsonApiSource.
 *
 * @see \Drupal\content_packager\Plugin\SourcePluginManager
 * @see \Drupal\content_packager\Plugin\PackageSourceInterface
 * @see \Drupal\content_packager\Plugin\SourcePluginSettingsBase
 * @see \Drupal\content_packager\Plugin\source\JsonApiSource
 * @see plugin_api
 *
 * @Annotation
 */
class PackageSource extends Plugin {
  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The title of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * An array of forms provided by this plugin.
   *
   * Key-value pairs of classes that implement PluginWithFormsInterface.
   * Keys used will be 'package' and 'configure'.
   *
   * @var \Drupal\Core\Plugin\PluginWithFormsInterface[]
   * @see \Drupal\content_packager\Plugin\source\RestPackageSource
   *
   * @see \Drupal\content_packager\Plugin\source\JsonApiPackageSource
   */
  public $forms = [];

}
